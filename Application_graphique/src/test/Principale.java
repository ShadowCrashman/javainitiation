package test;

import javax.swing.SwingUtilities;

public class Principale {
	
	// Point d'entrée exécuté par le thread main
	public static void main(String[] args) {
		SwingUtilities.invokeLater( () ->
			new Ecran().afficher()
			);
	}
}
