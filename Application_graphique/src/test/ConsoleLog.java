package test;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JMenuItem;

public class ConsoleLog implements ActionListener
{

	public void actionPerformed(ActionEvent e)
	{
		String message;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
		message = sdf.format(new Date());
		message += " clic sur le ";
		
		if (e.getSource() instanceof JButton) message += "bouton ";
		if (e.getSource() instanceof JMenuItem) message += "menu ";
		
		message += ((AbstractButton) e.getSource()).getText();
		System.out.println(message);
	}
}
