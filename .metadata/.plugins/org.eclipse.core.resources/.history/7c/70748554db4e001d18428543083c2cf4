package _06_Methodes;

/*
 * Une méthode représente un bloc d'instructions réutilisable.
 * Elle permet d'éviter les répétitions de code.
 * Une méthode se différencie d'une fonction en ce qu'elle est nécessairement rattachée à un objet.
 * Etant purement objet, Java ne supporte que le concept de méthode.
 * 
 * Déclaration (signature) :
 * 
 * Visibilité [mot-clé] type-retour nomMethode(liste des paramètres) { instructions }
 * 
 * Une méthode peut avoir plusieurs signatures/prototypes (avec différents paramètres)
 * => on parle alors de "surcharge" de méthode
 */

public class Methodes {

	/**
	 * Méthode afficher
	 */
	public static void afficher() {
		System.out.println("Méthode afficher");
	}

	/**
	 * Méthode afficher surchargée
	 * @param message
	 */
	public static void afficher(String message) {
		System.out.println(message);
	}
	
	/**
	 * Méthode afficher surchargée avec un tableau d'entiers
	 * @param tab
	 */
	public static void afficher(int[] tab) {
		System.out.println("\nMéthode afficher tableau d'entiers");
		
		for(int i = 0; i < tab.length; i++)
		{
			System.out.println(tab[i]);
		}
	}
	
	public static int somme(int a, int b) {
		return a + b;
	}

	public static int sommeTab(int[] tab) {
		int result = 0;
		
		for(int item: tab) result += item;
		
		return result;
	}

	public static void main(String[] args) {
		afficher();
		afficher("Méthode afficher surchargé avec un paramètre");
		int[] tab = {10,20,30};
		afficher(tab);
		afficher("Somme(2,3) = " + somme(2,3));
	}
}
