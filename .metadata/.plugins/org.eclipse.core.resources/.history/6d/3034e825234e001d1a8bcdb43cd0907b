package _03_Types._02_Types_complexes;

/*
 * Contrairement aux types primitifs, les types complexes (références, objets) exposent des méthodes;
 * Ils se distinguent visuellement des types primitifs en ce qu'ils commencent par une majuscule (PascalCase)
 * 
 * Les variables basées sur des types "objets" sont des pointeurs référençant les adresses mémoires desdits objets.
 * Leur valeur par défaut est "null".
 */

public class _01_Strings {

	public static void main(String[] args) {
		
		String firstName = "riri";
		String lastName = "duck";
		
		String fullName = firstName + " " + lastName; //Concaténation de chaines de caractères via l'opérateur "+"
		
		/*
		 * La classe String est implémentée de manière à produire des objets immuables (immutables)
		 * => Une fois produite, le contenu de la chaine de caractères n'est plus modifiable.
		 * De nombreuses méthodes permettent néanmoins de produire de nouvelles chaines "modifiées" à partir d'une chaine originale
		 * Ces méthodes retournent une chaine de caractères qui pourra être réalloué à la chaine originale
		 */
		
		System.out.println(firstName + " " + lastName.toUpperCase());
		
		String syllabe = "ri";
		
		String joined = syllabe + syllabe;
		
		if (firstName == joined) {
			System.out.println("Les 2 chaines pointent vers la même adresse");
		}
		else {
			System.out.println("Les 2 chaines pointent vers des adresses différentes");
		}
		
		if (firstName.equals(joined)) {
			System.out.println("Les 2 chaines ont des contenus identiques");
		}
		else {
			System.out.println("Les 2 chaines ont des contenus différents");
		}
	}
}
