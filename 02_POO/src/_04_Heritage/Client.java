package _04_Heritage;

public class Client extends User {

	private String phone;
	
	private boolean subscriber;

	// Getters And Setters
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public boolean isSubscriber() {
		return subscriber;
	}

	public void setSubscriber(boolean subscriber) {
		this.subscriber = subscriber;
	}

	// Constructeurs
	public Client(boolean subscriber) {
		super();
		this.subscriber = subscriber;
	}

	public Client(String prenom, String nom, int age) {
		super(prenom, nom, age);
	}

	public Client(String prenom, String nom, int age, String phone) {
		this(prenom, nom, age);
		
		this.setPhone(phone);
	}

	@Override
	public String toString() {
		return super.toString() + ", est client.e. Son numéro est " + this.getPhone();
	}
	
}
