package _02_Getters_Setters;

public class App {

	public static void main(String[] args) {

		User user = new User("Duck", "Fifi", 12);

		user.getAge();
		user.setAge(-12);
		
		System.out.println(user);
	}

}
