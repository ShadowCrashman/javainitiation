package _02_Getters_Setters;

public class User {

	// Membres d'instances
	private String nom;
	private String prenom;
	private int age;

	// Membre de classe
	private static int nUsers;
	
	//Getters and setters
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	
	public int getAge() {
		return this.age;
	}
	public void setAge(int age) {
		if (age <= 0)
			System.out.println("Un age ne peut pas être négatif"); // En vrai : on lèverait une exception
		else
			this.age = age;
	}
	
	public static int getnUsers() {
		return nUsers;
	}
	
	// Constructeurs
	public User() {
		nUsers ++;
	}

	public User(String nom) {
		this();
		this.nom = nom;
	}

	public User(String nom, String prenom) {
		this(nom);
		this.prenom = prenom;
	}

	public User(String nom, String prenom, int age) {
		this(nom, prenom);
		this.age = age;
	}
	
	// Méthodes de classe
	public void printUser() {
		System.out.println("Nom : " + prenom + " " + nom + "\nAge : " + age);
	}
	
	public static void printUsersNumber() {
		System.out.println("Il existe " + nUsers + " utilisateurs.");
	}
	
	@Override
	public String toString() {
		if (age != 0)
			return prenom + " " + nom + ", " + age + " ans";
		else
			return prenom + " " + nom;
	}
	
}
