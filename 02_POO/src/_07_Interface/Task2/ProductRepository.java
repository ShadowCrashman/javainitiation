package _07_Interface.Task2;

import java.util.List;

public interface ProductRepository {

	/*
	 * C.R.U.D
	 * 
	 * C : create
	 * R : read
	 * U : update
	 * D : delete
	 */
	
	List<Product> getAll();
	
	void addProduct(Product p);
	
	void updateProduct(Product p);
	
	void deleteProduct(Product p);
}
