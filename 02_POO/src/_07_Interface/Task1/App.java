package _07_Interface.Task1;

public class App {

	public static void main(String[] args) {
		
		Animal chien = new Chien();
		Animal chat = new Chat();
		
		chien.communiquer();
		chat.communiquer();
	}
}
