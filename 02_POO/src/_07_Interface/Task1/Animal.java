package _07_Interface.Task1;

/*
 * Une interface est un contrat que toute classe qui implémente cette interface devra respecter.
 * En java, une interface n'est pas une classe, mais un ensemble d'exigences (de fonctionnalités)
 * pour les classes qui doivent s'y conformer.
 * Une interface se présente sous la forme d'un ensemble de méthodes abstraites.
 * Contrairement à une classe, une interface ne peut pas avoir de constructeur ou d'attributs.
 * Une interface ne peut compter que des champs statiques ou définis avec le mot clé "final"
 */

public interface Animal {

	public void communiquer();
}
