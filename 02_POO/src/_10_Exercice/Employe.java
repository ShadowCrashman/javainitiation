package _10_Exercice;

public abstract class Employe {

	protected String nom;
	protected String prenom;
	protected int age;
	protected String date;
	
	public String getNom()
	{
		return "L'employé(e) " + this.prenom + " " + this.nom;
	}
	
	public Employe(String prenom, String nom, int age, String date)
	{
		super();
		this.prenom = prenom;
		this.nom = nom;
		this.age = age;
		this.date = date;
	}
	
	public abstract double calculerSalaire();
	
}
