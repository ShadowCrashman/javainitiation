package _10_Exercice;

public class Manutentionnaire extends Employe {

	private int nombreHeures;

	private boolean aRisque;
	
	private final static double PRIME_RISQUE_MANU = 200.0;
	private final static double SALAIRE_HEURE = 65.0;
	
	public int getNombreHeures() {
		return nombreHeures;
	}

	public void setNombreHeures(int nombreHeures) {
		this.nombreHeures = nombreHeures;
	}

	public boolean isaRisque() {
		return aRisque;
	}

	public void setaRisque(boolean aRisque) {
		this.aRisque = aRisque;
	}

	public Manutentionnaire(String prenom, String nom, int age, String date, int nombreHeures, boolean aRisque) {
		super(prenom, nom, age, date);
		this.nombreHeures = nombreHeures;
		this.aRisque = aRisque;
	}

	@Override
	public String getNom()
	{
		return "Le/la manutentionnaire " + super.prenom + " " + super.nom;
	}

	@Override
	public double calculerSalaire() {
		return nombreHeures * SALAIRE_HEURE + (aRisque ? PRIME_RISQUE_MANU : 0);
	}
}
