package _10_Exercice;

public class Technicien extends Employe {

	private int nombreUnites;

	private boolean aRisque;

	private final static double PRIME_RISQUE_TECHNICIEN = 200.0;
	private final static double SALAIRE_UNITAIRE = 5.0;
	
	public int getNombreUnites() {
		return nombreUnites;
	}

	public void setNombreUnites(int nombreUnites) {
		this.nombreUnites = nombreUnites;
	}

	public boolean isaRisque() {
		return aRisque;
	}

	public void setaRisque(boolean aRisque) {
		this.aRisque = aRisque;
	}

	public Technicien(String prenom, String nom, int age, String date, int nombreUnites, boolean aRisque) {
		super(prenom, nom, age, date);
		this.nombreUnites = nombreUnites;
		this.aRisque = aRisque;
	}

	@Override
	public String getNom()
	{
		return "Le/la technicien(ne) " + super.prenom + " " + super.nom;
	}
	
	@Override
	public double calculerSalaire() {
		return nombreUnites * SALAIRE_UNITAIRE + (aRisque ? PRIME_RISQUE_TECHNICIEN : 0);
	}

}
