package _10_Exercice;

public class Vendeur extends Employe {

	private double chiffreAffaire;
	
	private final static double POURCENTAGE_VENDEUR = 0.2;
	
	private final static double FIXE_VENDEUR = 400;

	public double getChiffreAffaire() {
		return chiffreAffaire;
	}

	public void setChiffreAffaire(double chiffreAffaire) {
		this.chiffreAffaire = chiffreAffaire;
	}

	public Vendeur(String prenom, String nom, int age, String date, double chiffreAffaire) {
		super(prenom, nom, age, date);
		this.chiffreAffaire = chiffreAffaire;
	}

	@Override
	public String getNom()
	{
		return "Le/la vendeur(se) " + super.prenom + " " + super.nom;
	}
	
	@Override
	public double calculerSalaire() {
		return this.chiffreAffaire * POURCENTAGE_VENDEUR + FIXE_VENDEUR;
	}

}
