package _10_Exercice;

public interface IPersonnel {

	void ajouterEmploye(Employe e);
	
	void calculerSalaires();
	
	double salaireMoyen();
	
	public void afficherEmployes();
}
