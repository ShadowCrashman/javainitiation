package _10_Exercice;

import java.util.ArrayList;
import java.util.List;

public class Personnel implements IPersonnel {

	private List<Employe> employes;

	public Personnel() {
		super();
		employes = new ArrayList<Employe>();
	}

	public Personnel(List<Employe> employes) {
		super();
		this.employes = employes;
	}

	public void ajouterEmploye(Employe employe)
	{
		employes.add(employe);
	}

	public void calculerSalaires()
	{
		for (Employe employe : employes)
		{
			System.out.println(employe.getNom() + " gagne " + employe.calculerSalaire() + "€.");
		}
	}
	
	public double salaireMoyen()
	{
		double result = 0.0;
		for (int i = 0; i < employes.size(); i++)
		{
			result += employes.get(i).calculerSalaire();
		}
		result /= employes.size();
		return result;
	}
	
	public void afficherEmployes()
	{
		for (Employe e : employes)
		{
			System.out.println(e.getNom() + ", " + e.age + " ans, est présent(e) depuis " + e.date);
		}
	}
	
}
