package _10_Exercice;

public class Representant extends Employe {

	private double chiffreAffaire;
	
	private final static double POURCENTAGE_REPRESENTANT = 0.2;
	
	private final static double FIXE_REPRESENTANT = 800;

	public Representant(String prenom, String nom, int age, String date, double chiffreAffaire) {
		super(prenom, nom, age, date);
		this.chiffreAffaire = chiffreAffaire;
	}

	@Override
	public String getNom()
	{
		return "Le/la représentant(e) " + super.prenom + " " + super.nom;
	}
	
	@Override
	public double calculerSalaire() {
		return this.chiffreAffaire * POURCENTAGE_REPRESENTANT + FIXE_REPRESENTANT;
	}

}
