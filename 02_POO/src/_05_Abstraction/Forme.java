package _05_Abstraction;

public abstract class Forme {

	public abstract double surface();
}
