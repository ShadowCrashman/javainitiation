package _05_Abstraction;

public class App {

	public static void main(String[] args) {
		
		Carre carre = new Carre(5);
		Cercle cercle = new Cercle(5);
		
		System.out.println("Surface du carré : " + carre.surface());
		System.out.println("Surface du cercle : " + cercle.surface());
	}
}
