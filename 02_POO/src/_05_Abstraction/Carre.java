package _05_Abstraction;

public class Carre extends Forme {

	private int cote;
	
	
	public int getCote() {
		return cote;
	}
	public void setCote(int cote) {
		if (cote < 0) throw new IllegalArgumentException("le côté du carré doit être positif");
		this.cote = cote;
			
	}


	public Carre(int cote) {
		super();
		this.cote = cote;
	}
	
	@Override
	public double surface()
	{
		return cote*cote;
	}
}
