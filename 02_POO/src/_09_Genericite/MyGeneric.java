package _09_Genericite;

/*
 * Le concept de généricité peut s'appliquer à des classes, des méthodes et des interfaces :
 * - identiques sur le plan algorithmique
 * - mais qui manipulent des types de données différents
 * 
 * Intérêt :
 * - optimisation du code
 * - moins de cast (transtypage) à faire
 * - moins de risques d'erreur à l'exécution
 */

public class MyGeneric<T> {

	private T data;

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public MyGeneric(T data) {
		super();
		this.data = data;
	}

	@Override
	public String toString() {
		return "MyGeneric [data=" + data + "]";
	}
	
	boolean myEquals(MyGeneric<T> other)
	{
		return data.equals(other.data);
	}
}
