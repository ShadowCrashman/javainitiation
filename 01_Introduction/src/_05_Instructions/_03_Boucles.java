package _05_Instructions;

import java.util.Scanner;

/*
 * Une boucle permet de répéter (itérer) un certain nombre de fois une instruction ou un bloc d'instructions.
 * 
 * Trois mots clés permettent de créer des boucles :
 * - for
 * - while
 * - do while
 */
public class _03_Boucles {

	public static void main(String[] args) {
		
		boucleFor();

		System.out.println("\n");
		
		boucleWhile();

		System.out.println("\n");
		
		boucleDoWhile();
		
	}
	
	public static void boucleFor() {
		
		/*
		 * La boucle for permet de répéter un blocs d'instructions un nombre déterminé de fois
		 * 
		 * Syntaxe : for(initExpr; testExpr; incExpr) { bloc d'instructions }
		 * 
		 * Avec :
		 * - initExpr : expression d'initialisation
		 * - testExpr : expression de test
		 * - incExpr : expression d'incrémentation
		 */
		
		String[] months = {"Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet",
								"Aout", "Septembre", "Octobre", "Novembre", "Décembre"};
		
		System.out.println("Boucle for simple :");
		for (int index = 0; index < months.length; index++) {
			System.out.print(months[index] + " ");
		}

		System.out.println();
		System.out.println("Boucle for avec break :");
		for (int index = 0; index < months.length; index++) {
			System.out.print(months[index] + " ");
			if (months[index] == "Octobre") break;
		}
		
		System.out.println();
		System.out.println("Boucle for avec continue :");
		for (int index = 0; index < months.length; index++) {
			if (months[index] == "Juillet") continue;
			System.out.print(months[index] + " ");
		}
	}
	
	public static void boucleWhile() {
		
		/*
		 * La boucle while permet de répéter un bloc d'instructions tant qu'une condition est vérifiée.
		 * 
		 * Syntaxe : while(testExpr) {bloc d'instructions à itérer}
		 * 
		 * Avec : testExpr : expression de test
		 */
		
		Scanner clavier = new Scanner(System.in);
		
		System.out.println("Entrez votre age :");
		
		int age = clavier.nextInt();
		
		while(age <= 0) {
			System.out.println("Entrez un age positif :");
			age = clavier.nextInt();
		}

		System.out.println("Vous avez " + age + " ans.");
		
		clavier.close();
	}
	
	public static void boucleDoWhile() {
		
		/*
		 * La boucle Do While se différencie de la boucle While en ce que la condition vérifiée est vérifiée
		 * après l'exécution du bloc d'instruction.
		 * On est donc certain d'exécuter au moins une fois le bloc d'instruction
		 */
		
		Scanner clavier = new Scanner(System.in);
		
		int age = 0;
		
		do {
			System.out.println("Entrez l'age de votre chien :");
			age = clavier.nextInt();
		}
		while(age <= 0);

		System.out.println("Votre chien a " + age + " ans.");
		
		clavier.close();
		
	}
}
