package _05_Instructions;

public class _01_General {

	public static void main(String[] args) {
		 
		/*
		 * Une instruction simple se termine par un ";"
		 * 
		 * Un bloc d'instruction est contenu entre accolades "{ bloc }"
		 * 
		 * Les variables déclarées dans un bloc d'instruction ne sont accessibles que dans ce bloc
		 */
		
		System.out.println("Cette instruction se termine forcément par un \";\"");
		
		{
			System.out.println("Ceci est la première instruction d'un bloc d'instructions");
			
			int a = 10; // la variable a n'est accessible que dans ce bloc d'instructions
			
			System.out.println("Ceci est la dernière instruction d'un bloc d'instructions");
		}
		
		
	}
}
