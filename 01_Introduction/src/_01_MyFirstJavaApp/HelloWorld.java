package _01_MyFirstJavaApp;

/**
 * 
 * @author Admin
 *
 */
public class HelloWorld {

	/**
	 * Ceci est un commentaire qui sera utilisé pour générer de la documentation
	 * @param args
	 */
	public static void main(String[] args) {

		// Ceci est un commentaire monoligne
		
		/* 
		 * Ceci est
		 * un commentaire
		 * multiligne
		 */
		
		System.out.println("Hello World !");

	}

}
