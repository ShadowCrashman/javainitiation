package _02_Entrees_Sorties;

import java.util.Scanner;

public class _02_Scan {

	public static void main(String[] args) {
		
		Scanner clavier = new Scanner(System.in);
		
		System.out.println("Que voulez-vous tester ? 1=int, 2=double, 3=string");
		int n = clavier.nextInt();
		
		switch(n) {
		case 1:
			printInt(clavier);
			break;
		case 2:
			printDouble(clavier);
			break;
		case 3:
			printString(clavier);
			break;
		default:
			break;
		}
		
		clavier.close();
	}

	public static void printInt(Scanner clavier) {
		System.out.println("Entrer un nombre entier : ");
		int n = clavier.nextInt();
		System.out.println("Le carré de " + n + " vaut " + n * n);
	}

	public static void printDouble(Scanner clavier) {
		System.out.println("Entrer un nombre à virgule flottante : ");
		double d = clavier.nextDouble();
		System.out.println("Vous avez entré " + d);
	}

	public static void printString(Scanner clavier) {
		System.out.println("Entrer une phrase svp... ");
		String str1 = clavier.next();
		String str2 = clavier.nextLine();
		System.out.println("Vous avez entré \"" + str1 + str2 + "\"");
	}
}
