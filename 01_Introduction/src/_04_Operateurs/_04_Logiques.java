package _04_Operateurs;

public class _04_Logiques {

	public static void main(String[] args) {
		
		/*
		 * Les opérateurs logiques s'appliquent à des opérandes de type boolean et produisent un résultat boolean
		 * 
		 * Java comporte trois opérateurs logiques (dont un unaire)
		 * 
		 * - ET : A && B => true si A ET B sont vrais, false sinon
		 * - OU : A || B => true si A OU B est vrai, false sinon
		 * - NON : !A => true si A est false, false si A est true
		 */
		
		boolean A = true, B = false;
		
		if (A && B) {
			System.out.println("A et B sont vrais");
		}
		else {
			System.out.println("A et B ne sont pas tous les deux vrais");
		}

		if (A || B) {
			System.out.println("A ou B est vrai");
		}

		if (A ^ B) {
			System.out.println("Seul A ou seul B est vrai");
		}

		if (!A) {
			System.out.println("A n'est pas vrai");
		}
	}
}
