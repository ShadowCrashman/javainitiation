package _04_Operateurs;

public class _02_Affectation {

	public static void main(String[] args) {
		
		int c = 10;
		
		System.out.println(c);
		System.out.println(c++);
		System.out.println(++c);
		
		int a = 0;
		
		a += 5;
		System.out.println("a = " + a);
		
		a *= 5;
		System.out.println("a = " + a);
		
		a /= 5;
		System.out.println("a = " + a);
		
		a -= 5;
		System.out.println("a = " + a);
		
	}
}
