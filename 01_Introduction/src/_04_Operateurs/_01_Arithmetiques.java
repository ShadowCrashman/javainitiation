package _04_Operateurs;

public class _01_Arithmetiques {

	public static void main(String[] args) {
		
		int v1 = 10;
		int v2;
		
		v2 = 4;
		
		System.out.println(v1 + " + " + v2 + " = " + (v1 + v2));
		System.out.println(v1 + " - " + v2 + " = " + (v1 - v2));
		System.out.println(v1 + " * " + v2 + " = " + (v1 * v2));
		System.out.println(v1 + " / " + v2 + " = " + (v1 / v2));
		System.out.println(v1 + " % " + v2 + " = " + (v1 % v2));
	}
}
