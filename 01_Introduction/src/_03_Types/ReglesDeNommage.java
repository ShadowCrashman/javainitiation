package _03_Types;

public class ReglesDeNommage {

	public static void main(String[] args) {
		/*
		 * Une variable peut commencer au choix par :
		 * - une lettre
		 * - un "_"
		 * - le caractère "$"
		 * mais pas par un chiffre ou un opérateur
		 */
		
		int monEntier = 10;
		
		String _maChaineDeCaractere = "ma chaine de caractères";
		
		char $monCaractere = 'a';
		
		// double 00monDouble;	Interdit
		// double +monDouble;	Interdit
		
		/*
		 * Par convention, on utilse le camelCase pour les variables
		 * 
		 * - camelCase
		 * - PascalCase
		 * - snake_case
		 */
	}

}
