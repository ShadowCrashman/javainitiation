package _03_Types._02_Types_complexes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class _04_Collections {

	public static void main(String[] args) {

		List<String> list = new ArrayList<String>();
		
		list.add("a");
		list.add("b");
		list.add("c");
		list.add("d");
		list.add("e");
		list.add("f");
		
		System.out.println("Boucle for :");
		for (int i = 0; i < list.size(); i++) {
			System.out.print(list.get(i));
			if (i+1 < list.size()) System.out.print(", ");
		}

		System.out.println("\n\nBoucle foreach :");
		for (String str : list) {
			System.out.print(str);
			if (str != list.get(list.size()-1)) System.out.print(", ");
		}
		
		/*
		 * La classe "Collections" offre plusieurs méthodes utilitaires pour les objets de type collection
		 */
		
		System.out.println("\n\n" + list + " (list)");
		Collections.shuffle(list);
		System.out.println(list + " (shuffle)");
		Collections.sort(list);
		System.out.println(list + " (sort)");
		Collections.rotate(list, 1);
		System.out.println(list + " (rotate 1)");
		Collections.rotate(list, -3);
		System.out.println(list + " (rotate -3)");
		Collections.sort(list);
		Collections.reverse(list);
		System.out.println(list + " (reverse)");
		
		List<String> sousListe = list.subList(2, 5);
		System.out.println(sousListe + "(list.subList(2, 5))");

		System.out.println("\nTableau associatif");
		/*
		 * Un tableau associatif (parfois appelé dictionnaire ou "map") permet d'associer une clé à une valeur.
		 * Un tableau associatif ne peut pas contenir de doublon de clé.
		 */
		
		Map<Integer, String> map = new TreeMap<Integer, String>();
		// Map<Integer, String> map = new HashMap<Integer, String>();
		// Map<Integer, String> map = new LinkedHashMap<Integer, String>();
		
		map.put(1, "un");
		map.put(2, "deux");
		map.put(3, "trois");
		map.put(4, "quatre");
		map.put(5, "cinq");
		map.put(1, "doublon"); // la clé existe déjà : remplace l'entrée par la nouvelle valeur
		
		// keySet() : retourne un objet de type Set<> représentant la liste des clés contenues dans la collection
		for(Integer item : map.keySet()) {
			System.out.print(item + " ");
		}
		
		System.out.println();
		
		// entrySet() : retourne un objet de type Map.Entry<,> représentant la liste des éléments contenus dans la collection
		for(Map.Entry<Integer, String> entry : map.entrySet()) {
			Integer key = entry.getKey();
			String value = entry.getValue();
			System.out.println(key + "| " + value);
		}
		
		map.remove(2);
		
		System.out.println(map + "(après la suppresion de la clé 2");
		
		
	}

}
