package _03_Types._02_Types_complexes;

/*
 * Contrairement aux types primitifs, les types complexes (références, objets) exposent des méthodes;
 * Ils se distinguent visuellement des types primitifs en ce qu'ils commencent par une majuscule (PascalCase)
 * 
 * Les variables basées sur des types "objets" sont des pointeurs référençant les adresses mémoires desdits objets.
 * Leur valeur par défaut est "null".
 */

public class _01_Strings {

	public static void main(String[] args) {
		
		String firstName = "riri";
		String lastName = "duck";
		
		String fullName = firstName + " " + lastName; //Concaténation de chaines de caractères via l'opérateur "+"
		
		/*
		 * La classe String est implémentée de manière à produire des objets immuables (immutables)
		 * => Une fois produite, le contenu de la chaine de caractères n'est plus modifiable.
		 * De nombreuses méthodes permettent néanmoins de produire de nouvelles chaines "modifiées" à partir d'une chaine originale
		 * Ces méthodes retournent une chaine de caractères qui pourra être réalloué à la chaine originale
		 */
		
		System.out.println(firstName + " " + lastName.toUpperCase());
		
		String syllabe = "ri";
		
		String joined = syllabe + syllabe;
		
		if (firstName == joined) {
			System.out.println("Les 2 chaines pointent vers la même adresse");
		}
		else {
			System.out.println("Les 2 chaines pointent vers des adresses différentes");
		}
		
		if (firstName.equals(joined)) {
			System.out.println("Les 2 chaines ont des contenus identiques");
		}
		else {
			System.out.println("Les 2 chaines ont des contenus différents");
		}
		
		String s1 = "Bonjour";
		String s2 = "Bonjour";
		String s3 = new String("Bonjour");
		
		System.out.println("s1 == s2 ? " + (s1 == s2));
		System.out.println("s1 == s3 ? " + (s1 == s3));
		
		System.out.println("s1 equals s3 ? " + s1.equals(s3));
		
		byte cara = 2;
		
		System.out.println((cara +1) + "e caractère de s1 ? " + s1.charAt(cara));
		
		System.out.println(s1.concat(" Riri"));
		
		System.out.println(s1.substring(3));
		System.out.println(s1.substring(0, 3));
		
		String chaine = "Ceci est une chaine de caractères";
		
		System.out.println(chaine);
		
		String[] splitted = chaine.split(" ");

		for (String item : splitted) {
			if (item != splitted[0]) {
				System.out.print(" - ");
			}
			System.out.print(item);
		}
		
		String replacement = chaine.replace("Ceci", "Ce truc");
		System.out.println("\n" + replacement);
		
		replacement = chaine.replace('e', 'u');
		System.out.println("\n" + replacement);
		
		/*
		 * Conversion de types primitifs en chaine de caractères
		 * 
		 * Il est possible de convertir des valeurs numériques en chaines de caractères, et réciproquement.
		 * 
		 * Pour ce faire, il faut faire appel à la classe englobante du type primitif considéré.
		 */
		
		String myStr = "123";
		int myInt = Integer.parseInt(myStr);
		
		System.out.println(myInt*2);
		
		myStr = "3.141519";
		double myDouble = Double.parseDouble(myStr);
		
		System.out.println(myDouble*2);
		
		/*
		 * Contrairement à la classe String, la classe StringBuilder permet de créer des chaines de caractères modifiables.
		 * Il n'est donc pas nécessaire de réallouer de la mémoire à chaque fois qu'on effectue une modification.
		 */
		
		StringBuilder builder = new StringBuilder("Ceci est une chaine de caractères créée avec un StringBuilder.");
		
		System.out.println(builder);
		
		builder.append(" Il est donc possible de la modifier");
		
		System.out.println(builder);
		
	}
}
