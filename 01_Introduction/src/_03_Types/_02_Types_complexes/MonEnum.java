package _03_Types._02_Types_complexes;

/*
 * Un type énuméré est un type de données comportant un ensemble fini d'états (ou de valeurs) auxquels sont associés un nom.
 * Un type énuméré doit être déclaré dans un fichier séparé portant le nom de l'enum
 * Un type énuméré s'introduit avec le mot clé "enum"
 * Conventionnellement :
 * - le nom du type énuméré commence par une majuscule (PascalCase)
 * - les noms d'états sont en MAJUSCULES
 * - Utilisation du "_" pour les mots composés
 */

public enum MonEnum {
	VERT, ORANGE, ROUGE, ROUGE_FONCE
}
