package _03_Types._01_Types_primitifs;

public class _02_Entiers {

	public static void main(String[] args) {
		
		/*
		 * Il existe 4 types d'entier : byte, short, int, long
		 * 
		 * Ces 4 types se différencient par la taille (en nombre d'octets) des variables qu'ils permettent de stocker
		 */
		
		byte b = 127;
		
		short s = 32_767;
		
		int i = 2_000_000_000;
		
		long l = 1_000_000_000_000_000_000L;
		
		/*
		 * A chaque type primitif est associé un type complexe (sous forme de classe envoloppante "Wrapper")
		 * proposant des propriétés et des méthodes utilitaires
		 * 
		 * Type primitif	=>	Type complexe
		 * - byte			=>	Byte
		 * - short			=>	Short
		 * - int			=>	Integer
		 * - long			=>	Long
		 * - float			=>	Float
		 * - long			=>	Double
		 * - char			=>	Character
		 * - boolean		=>	Boolean
		 */
		
		System.out.printf(" - %s (%d bits) from %d to %d\n", Byte.TYPE, Byte.SIZE, Byte.MIN_VALUE, Byte.MAX_VALUE);
		System.out.printf(" - %s (%d bits) from %d to %d\n", Short.TYPE, Short.SIZE, Short.MIN_VALUE, Short.MAX_VALUE);
		System.out.printf(" - %s (%d bits) from %d to %d\n", Integer.TYPE, Integer.SIZE, Integer.MIN_VALUE, Integer.MAX_VALUE);
		System.out.printf(" - %s (%d bits) from %d to %d\n", Long.TYPE, Long.SIZE, Long.MIN_VALUE, Long.MAX_VALUE);
		
	}
}
