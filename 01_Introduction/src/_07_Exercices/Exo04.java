package _07_Exercices;

import java.util.Scanner;

/*
 * Vérifier si un nombre est divisible par 3 et 13 ou non.
 * Un nombre entier est divisible par un autre quand le résultat de la division euclidienne est un entier sans reste.
 * On peut écrire b = k*a + R, avec R = 0.
 */
public class Exo04 {

	public static void main(String[] args) {

		Scanner clavier = new Scanner(System.in);
		
		System.out.println("Entrez une valeur :");
		
		int n = clavier.nextInt();

		if (n%3 == 0 && n%13 == 0) System.out.println(n + " est divisible par 3 et par 13");
		else if (n%3 == 0) System.out.println(n + " est divisible par 3");
		else if (n%13 == 0) System.out.println(n + " est divisible par 13");
		else System.out.println(n + " n'est divisible ni par 3, ni par 13");
		
		clavier.close();
	}

}
