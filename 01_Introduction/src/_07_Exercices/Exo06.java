package _07_Exercices;

import java.util.Scanner;

/*
 * L'utilisateur doit deviner un nombre secret (généré automatiquement) entre 1 et 1000.
 * On va donc demander à l'utilisateur de trouver ce nombre secret.
 * Tant qu'il n'a pas trouvé ce nombre on lui demandera encore et encore, jusqu'à ce qu'il le trouve.
 * Si l'utilisateur choisit un nombre trop petit, l'application lui dira que le nombre qu’il a rentré est trop petit
 * Si l'utilisateur choisit un nombre trop grand, l'application lui dira que le nombre qu’il a rentré est trop grand
 * Si l'utilisateur trouve le nombre recherché, l'application lui indiquera le nombre d'essais dont il a eu besoin
 * 
 * Exemple :
 * Entrée : Entrer un nombre entre 1 et 1000 (1 - 1000) : 500
 * Sorties possibles :
 * Votre nombre est trop grand !
 * Votre nombre est trop petit !
 * Trouvé en 8 essais ! Bien Joué !
 */
public class Exo06 {

	public static void main(String[] args) {

		Scanner clavier = new Scanner(System.in);
		
		int nombreMystere = (int) (Math.random() * 999 + 1);
		int essai = 0;
		
		System.out.println("Entrez un nombre entre 1 et 1000 :");
		
		int n = clavier.nextInt();

		while(n != nombreMystere)
		{
			String t = "";
			if (Math.abs(n - nombreMystere) > 100) t = "beaucoup ";
			if (n > nombreMystere) System.out.println("Votre nombre est "+t+"trop grand !");
			else System.out.println("Votre nombre est "+t+"trop petit !");
			essai++;
			n = clavier.nextInt();
		}

		System.out.println(n + " : Trouvé en " + essai + " essais ! Bien joué !");
		
		
		clavier.close();
	}

}
