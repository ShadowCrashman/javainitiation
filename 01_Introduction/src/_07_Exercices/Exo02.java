package _07_Exercices;

import java.util.Scanner;

//Exercice 2 : Écrire un programme qui demande à l'utilisateur de rentrer un nombre entier et qui retourne sa racine carrée.
public class Exo02 {

	public static void main(String[] args) {

		Scanner clavier = new Scanner(System.in);
		
		System.out.println("Entrez une valeur :");
		
		int n = clavier.nextInt();
		
		System.out.println("La racine carrée de votre valeur " + n + " est " + Math.sqrt(n));
		
		clavier.close();
	}

}
