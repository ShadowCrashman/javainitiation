package _07_Exercices;

/*
 * 
 * En mathématiques, la suite de Fibonacci est une suite d'entiers dans laquelle chaque terme est la somme des deux termes qui le précèdent.
 * Elle commence par les termes 0 et 1 si on part de l'indice 0, ou par 1 et 1 si on part de l'indice 1.
 * Dans les exercices suivants, nous nous concentrerons sur des tableaux à une dimension. Nous vous montrerons comment implémenter et utiliser des tableaux.
 * 
 * Exemple : 1 1 2 3 5 8 13 21 34….
 * 
 * Notée (Fn), elle est donc définie par :
 * 
 * F0 = 0,
 * F1 = 1 et
 * Fn = Fn-1 + Fn-2 pour n >= 2
 * 
 * On peut définir cette suite par récurrence.
 * 
 * On initialise les formules avec deux conditions initiales : F0 = 0, F1 = 1
 * Formule de récurrence : Fn = Fn-1 + Fn-2 pour n >= 2
 */
public class Exo12 {

	public static void main(String[] args) {

		int[] suite = new int[15];
		suite[0] = 0;
		System.out.print(suite[0] + " ");
		suite[1] = 1;
		System.out.print(suite[1] + " ");
		
		for (int i = 2; i < suite.length; i++)
		{
			suite[i] = suite[i-1] + suite[i-2];
			System.out.print(suite[i] + " ");
		}
		
	}

}
