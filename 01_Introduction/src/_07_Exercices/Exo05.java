package _07_Exercices;

import java.util.Scanner;

/**
 * Demander à l'utilisateur de choisir un nombre entier et retourner sa factorielle.
 * Utiliser la récursivité.
 * n! = n*(n-1)! si n!=1
 * 1! = 1;
 */
public class Exo05 {

	public static void main(String[] args) {

		Scanner clavier = new Scanner(System.in);
		
		System.out.println("Entrez une année :");
		
		int n = clavier.nextInt();

		if ((n%4 == 0 && n%100 != 0) || n%400 == 0) System.out.println("L'année " + n + " est bissextile");
		else System.out.println("L'année " + n + " n'est pas bissextile");
		
		clavier.close();
	}

}
