package _07_Exercices;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Suppression d’un élément dans un tableau trié
 * 
 * La suppression d’un élément d’un tableau qui contient une liste de données décale vers la gauche les éléments situés à droite de l’élément à supprimer.
 * Le premier décalage écrase la valeur à supprimer par le contenu de la case de droite.
 * Les décalages successifs répètent cette opération sur les cases suivantes, jusqu’à la fin des données.
 * 
 * Créer et initialiser un tableau, puis supprimer un élément de ce tableau à la position spécifiée (de 0 à N-1).
 * Pour supprimer un élément du tableau, déplacez les éléments, juste après la position donnée vers une position à gauche et réduisez la taille du tableau.
 * Exemple :
 * 
 * Données d'entrée : Saisir le nombre de notes :
 * 
 * Note 1 : 8.5
 * Note 2 : 9.5
 * Note 3 : 11
 * Note 3 : 12.5
 * Note 4 : 18.0
 * Saisir la position de l'élément à supprimer : 2
 * Données de sortie : [8.5, 11.0, 12.5, 18.0]
 */
public class Exo08 {

	public static void main(String[] args) {

		Scanner clavier = new Scanner(System.in);
		
		List<Float> notes = new ArrayList<Float>();
		notes.add(8.5f);
		notes.add(9.5f);
		notes.add(11.0f);
		notes.add(12.5f);
		notes.add(18.0f);
		
		System.out.println("Saisir la position de la note à supprimer :");
		int n = clavier.nextInt();
		
		notes.remove(n);
		
		for(int i = 0; i < notes.size(); i++)
		{
			System.out.print(notes.get(i) + "  ");
		}
		
		
		clavier.close();
	}

}