package _07_Exercices;

import java.util.Scanner;

/**
 * Demander à l'utilisateur de choisir un nombre entier et retourner sa factorielle.
 * Utiliser la récursivité.
 * n! = n*(n-1)! si n!=1
 * 1! = 1;
 */
public class Exo10 {

	public static void main(String[] args) {

		Scanner clavier = new Scanner(System.in);
		
		System.out.println("Saisir un nombre :");
		int n = clavier.nextInt();
		
		System.out.println(n + "! = " + factoriel(n));
		
		clavier.close();
	}
	
	public static int factoriel(int value) {
		if (value == 0) return 1; //cas terminal
		return value*factoriel(value - 1);
	}


}
