package _07_Exercices;

//Permuter 2 variables
public class Exo01 {

	public static void main(String[] args) {

		int a = 5, b = 9;
		System.out.println("a et b valent respectivement :" + a + " et " + b);

		int bis = a;
		a = b;
		b = bis;
		
		System.out.println("a et b valent respectivement :" + a + " et " + b);
	}

}
