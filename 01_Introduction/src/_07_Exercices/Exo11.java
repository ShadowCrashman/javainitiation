package _07_Exercices;

import java.util.Scanner;

/* PGCD : Plus Grand Dénominateur Commun
*
* Appliquer l’Algorithme d’Euclide qui dit :
* si b divise a alors pgcd(a,b) = b
* sinon pgcd(a,b) = pgcd(b, a mod b)
* 
* où a mod b est le reste de la division de a par b.
* Ecrivez un programme permettant de trouver le plus PGCD à partir cet algorithme.
*/
public class Exo11 {

	public static void main(String[] args) {

		Scanner clavier = new Scanner(System.in);
		
		System.out.println("Saisir un premier nombre :");
		int a = clavier.nextInt();

		System.out.println("Saisir un deuxième nombre :");
		int b = clavier.nextInt();
		
		System.out.println("pgcd(" + a + ", " + b + ") = " + pgcd(a, b));
		
		clavier.close();
	}

	public static int pgcd(int a, int b) {
		if (a%b == 0) return b; //cas terminal
		return pgcd(b, a%b);
	}


}
