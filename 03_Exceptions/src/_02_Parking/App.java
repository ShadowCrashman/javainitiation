package _02_Parking;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class App {

	private static Logger logger = LogManager.getLogger(App.class);
	
	public static void main(String[] args) {
		
		Parking parc = new Parking(5);
		
		try {
			parc.remplir();
			System.out.println("Il reste " + parc.getNombreDePlacesDisponibles() + " places");
			
			parc.remplir();
			System.out.println("Il reste " + parc.getNombreDePlacesDisponibles() + " places");
			
			parc.remplir();
			System.out.println("Il reste " + parc.getNombreDePlacesDisponibles() + " places");
			
			parc.remplir();
			System.out.println("Il reste " + parc.getNombreDePlacesDisponibles() + " places");
			
			parc.remplir();
			System.out.println("Il reste " + parc.getNombreDePlacesDisponibles() + " places");
			
			parc.remplir();
			System.out.println("Il reste " + parc.getNombreDePlacesDisponibles() + " places");
		} catch (ParkingPleinException e) {
			// System.out.println(e.getMessage());
			logger.error(e.getMessage());
		}

	}
}
