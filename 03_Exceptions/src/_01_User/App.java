package _01_User;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class App {

	private static Logger logger = LogManager.getLogger(App.class);
	
	public static void main(String[] args) {
		
		//Le bloc "try" permet d'implémenter des instructions susceptibles de lever des exceptions
		try {

			User user = new User("Riri", "Duck", 12);
			System.out.println(user);
			user.setAge(13);
			System.out.println(user);
			user.setAge(-12);
			
		} catch (IllegalAgeException e) {

			//System.out.println(e.getMessage());
			logger.trace(e.getMessage());
			
		} catch (IllegalArgumentException e) {

			//System.out.println(e.getMessage());
			logger.warn(e.getMessage());
			
		} catch (Exception e) {

			//System.out.println(e.getMessage());
			logger.error(e.getMessage());
		}
		finally
		{
			System.out.println("Le bloc finally est exécuté quoiqu'il arrive, avec ou sans exception.");
			System.out.println("Il est souvent utilisé pour libérer des ressources (flux, connexion bdd)");
			
		}
		
	}
}
