package _01_User;

public class User {
	
	private String prenom;
	private String nom;
	private int age;
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) throws IllegalAgeException {
		if (age < 0)
		{
			//System.out.println("un age ne peut pas être négatif");
			//throw new IllegalArgumentException("un age ne peut pas être négatif");
			throw new IllegalAgeException();
		}
		this.age = age;
	}

	public User() {
		super();
	}
	public User(String prenom) {
		this();
		this.setNom(nom);
	}
	public User(String prenom, String nom) {
		this(prenom);
		this.setPrenom(prenom);
	}
	public User(String prenom, String nom, int age) throws IllegalAgeException {
		this(prenom, nom);
		this.setAge(age);
	}
	
	@Override
	public String toString() {
		return "User [prenom=" + prenom + ", nom=" + nom + ", age=" + age + "]";
	}
	
}
