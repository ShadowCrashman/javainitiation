package Model;

import java.io.Serializable;

public class Adresse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String voie;
	
	private String ville;
	
	// Mot clé "transient" : champ ignoré lors de la sérialisation de l'objet
	private transient int codePostal;

	public String getVoie() {
		return voie;
	}

	public void setVoie(String voie) {
		this.voie = voie;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public int getCodePostal() {
		return codePostal;
	}

	public void setCodePostal(int codePostal) {
		this.codePostal = codePostal;
	}

	// ATTENTION A NE PAS OUBLIER LE CONSTRUCTEUR SANS PARAMETRE POUR LES ENCODERS
	public Adresse() {
		super();
	}

	public Adresse(String voie, String ville, int codePostal) {
		super();
		this.voie = voie;
		this.ville = ville;
		this.codePostal = codePostal;
	}

	@Override
	public String toString() {
		return "Adresse [voie=" + voie + ", ville=" + ville + ", codePostal=" + codePostal + "]";
	}
	
	
}
