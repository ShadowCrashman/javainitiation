package App;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import Helpers.Binary;
import Helpers.XML;
import Model.Adresse;

public class App {

	public static void main(String[] args) {
		
		Adresse adresse = new Adresse("Arthur Webber", "Strasbourg", 67000);
		Adresse adresse2 = new Adresse("Place Kléber", "Strasbourg", 67000);
		
		ArrayList<Adresse> list = new ArrayList<Adresse>();
		list.add(adresse);
		list.add(adresse2);
		
		try {
			String path = "Exports/adresse.xml";
			XML.encodeToFile(adresse, path);
			Adresse adresseLue = (Adresse) XML.decodeFromFile(path);
			System.out.println(adresseLue);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			String path = "Exports/adresse.bin";
			Binary.encodeToFile(list, path);
			Object obj = Binary.decodeFromFile(path);
			
			if (obj instanceof ArrayList<?>)
			{
				ArrayList<?> arrayList = (ArrayList<?>) obj;
				
				for (int i = 0; i < arrayList.size(); i++)
				{
					System.out.println(arrayList.get(i));
				}
			}
			else
			{
				System.out.println("ça marche pas");
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
